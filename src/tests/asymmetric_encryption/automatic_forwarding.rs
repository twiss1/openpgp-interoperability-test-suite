use crate::{
    OpenPGP,
    Data,
    Result,
    data,
    tests::{
        Expectation,
        TestMatrix,
        ConsumerTest,
    },
};

/// Tests how automatically forwarded messages are handled.
pub struct AutomaticForwarding {
}

impl AutomaticForwarding {
    pub fn new() -> Result<AutomaticForwarding> {
        Ok(AutomaticForwarding {
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for AutomaticForwarding {
    fn title(&self) -> String {
        "Automatic forwarding".into()
    }

    fn description(&self) -> String {
        "<p>Tests automatically forwarded messages.</p>".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("TSK".into(), data::certificate("forwarded-secret.pgp").into())]
    }

    fn run(&self, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumerTest::run(self, implementations)
    }
}

impl ConsumerTest for AutomaticForwarding {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        Ok(vec![
            ("Forwarded message".into(),
             data::message("forwarded.asc").into(),
             None)
        ])
    }

    fn consume(&self, _i: usize, pgp: &dyn OpenPGP, artifact: &[u8])
               -> Result<Data> {
        pgp.decrypt(data::certificate("forwarded-secret.pgp"), artifact)
    }

    fn check_consumer(&self, _i: usize, artifact: &[u8]) -> Result<()> {
        const MESSAGE: &[u8] = b"Hello Bob, hello world";
        if &artifact[..] == MESSAGE {
            Ok(())
        } else {
            Err(anyhow::anyhow!("Expected {:?}, got {:?}", MESSAGE, artifact))
        }
    }

}
